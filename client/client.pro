#-------------------------------------------------
#
# Project created by QtCreator 2015-04-21T20:37:50
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp \
    camerawidget.cpp

HEADERS  += mainwindow.h \
    dialog.h \
    camerawidget.h

FORMS    += mainwindow.ui \
    dialog.ui \
    camerawidget.ui

	

RESOURCES += \
    src.qrc