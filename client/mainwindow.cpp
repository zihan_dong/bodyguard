#include <QtGui>
#include <QtNetwork/QTcpSocket>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QStringList>
#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    dia = new Dialog;
    dia->show();
    camera = new cameraWidget;
    class QValidator *validator=new QIntValidator(0,60,this);
    class QValidator *validator1=new QIntValidator(0,300,this);
    socket = new QTcpSocket(this);
    ui->id->setText("请输入卡号");
    QPalette palette;
    palette.setBrush(QPalette::Background,QBrush(QPixmap(":/back2.jpg").scaled(this->size())));
    this->setPalette(palette);

    this->setWindowTitle("智能婴儿房监控管理系统");//设置窗口名字
    this->setWindowIcon(QIcon(":/logo.ico"));//设置图标
    ui->tempEdit->setValidator(validator);
    ui->humiEdit->setValidator(validator);
    ui->lightEdit->setValidator(validator1);
    pixmap = new QPixmap();
    this->setFixedSize(660,472);
    timer = new QTimer(this);
    timer2 = new QTimer(this);
    ui->lcdNumber->setDigitCount(8);
    QTime t =time.currentTime();
    ui->lcdNumber->display(t.toString("hh:mm:ss"));
    timer->start(500);
    flag = 1;
    flag1 = 1;
    QDate nowdate = QDate::currentDate();
    ui->timeLabel->setText(nowdate.toString("yyyy/MM/d, dddd"));
    connect(timer,SIGNAL(timeout()),this,SLOT(updateTimeSlot()));
    connect(timer2,SIGNAL(timeout()),this,SLOT(on_pushButtonStart_clicked()));
    connect(dia,SIGNAL(mySignal(QString)),this,SLOT(changeWidgetSlot(QString)));
    connect(socket,SIGNAL(readyRead()),this,SLOT(slotRecvData()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateTimeSlot()
{
    QTime upt = time.currentTime();
    ui->lcdNumber->display(upt.toString("hh:mm:ss"));

    int temp = ui->templabel->text().toInt();
    int humi = ui->humilabel->text().toInt();
    int light = ui->lightlabel->text().toInt();
    int maxTemp = ui->maxtemp->text().toInt();
    int maxHumi = ui->maxhumi->text().toInt();
    int maxLight = ui->maxlight->text().toInt();
    if(temp>=maxTemp || humi>=maxHumi || light>=maxLight)
    {
        ui->showlabel->setStyleSheet("QLabel{background-color: rgb(255, 0, 0)}");
        if(temp>=maxTemp)
            ui->templabel->setStyleSheet("QLabel{color: rgb(255, 0, 0);}");
        if(humi>=maxHumi)
            ui->humilabel->setStyleSheet("QLabel{color: rgb(255, 0, 0);}");
        if(light>=maxLight)
            ui->lightlabel->setStyleSheet("QLabel{color: rgb(255, 0, 0);}");
    }
    else if((temp<maxTemp&&temp>(maxTemp-5)) || (humi<maxHumi&&humi>(maxHumi-5)) || (light<maxLight&&light>(maxLight-5)))
    {
        ui->showlabel->setStyleSheet("QLabel{background-color:  rgb(255, 255, 0)}");
    }
    if(temp<=(maxTemp-5) && humi<=(maxHumi-5) && light<=(maxLight-5))
    {
        ui->showlabel->setStyleSheet("QLabel{background-color: rgb(0, 170, 0)}");
    }
    if(temp<maxTemp&&temp>(maxTemp-5))
        ui->templabel->setStyleSheet("QLabel{color: rgb(255, 255, 0);}");
    if(humi<maxHumi&&humi>(maxHumi-5))
        ui->humilabel->setStyleSheet("QLabel{color: rgb(255, 255, 0);}");
    if(light<maxLight&&light>(maxLight-5))
         ui->lightlabel->setStyleSheet("QLabel{color: rgb(255, 255, 0);}");
    if(temp<=(maxTemp-5))
        ui->templabel->setStyleSheet("QLabel{color: rgb(0, 170, 0);}");
    if(humi<=(maxHumi-5))
        ui->humilabel->setStyleSheet("QLabel{color: rgb(0, 170, 0);}");
    if(light<=(maxLight-5))
        ui->lightlabel->setStyleSheet("QLabel{color: rgb(0, 170, 0);}");
}

void MainWindow::changeWidgetSlot(QString buf)
{

    this->show();
    dia->close();

    if("admin" != buf)
    {
        ui->fanBtnSet->setEnabled(false);
        ui->humiButton->setEnabled(false);
        ui->tempButton->setEnabled(false);
        ui->lightButton->setEnabled(false);
        ui->humiEdit->setEnabled(false);
        ui->tempEdit->setEnabled(false);
        ui->lightEdit->setEnabled(false);
    }
    else
    {
        ui->fanBtnSet->setEnabled(true);
        ui->humiButton->setEnabled(true);
        ui->tempButton->setEnabled(true);
        ui->lightButton->setEnabled(true);
        ui->humiEdit->setEnabled(true);
        ui->tempEdit->setEnabled(true);
        ui->lightEdit->setEnabled(true);
    }
}

void MainWindow::slotRecvData()
{
    static int i;
    QByteArray arr;
    QString buf;
    arr = socket->readAll();
    buf = QString(arr);
    qDebug()<<i++<<arr;
    QStringList strlist;
    strlist = buf.split(",");
    if("begin" == strlist.at(0))
    {
        ui->templabel->setText(strlist.at(1));
        ui->humilabel->setText(strlist.at(2));
        ui->lightlabel->setText(strlist.at(3));
    }
    else if("rid" == strlist.at(0))
    {
        if("ok" == strlist.at(1))
        {
            QMessageBox::information(this, "提示", "授权成功");
        }
        else if("double_err" == strlist.at(1))
        {
            QMessageBox::information(this, "提示", "授权失败!");
        }
        else if("del_ok" == strlist.at(1))
        {
            QMessageBox::information(this, "提示", "撤销授权成功!");
        }
        else if("del_err" == strlist.at(1))
        {
            QMessageBox::information(this, "提示", "撤销授权失败!");
        }
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::information(this,"提示","该系统的主要功能是采用Zigbee和串口对婴儿房的温度、湿度以及光照强度的采集，并且设置各个环境参数的上下限值，在测试参数超限时发出报警信号，同时系统可自动调节当前环境参数达到正常范围以内，也可人为发送控制指令用于调节当前环境参数。系统提供可视化的人机交互界面，用户可以通过客户端操作整个系统",QMessageBox::Yes);
}

void MainWindow::on_pushButtonStop_clicked()//不在显示,返回登录界面
{
    this->close();
    dia->show();
}

void MainWindow::on_pushButtonStart_clicked()// 显示 温湿度等
{
    if(flag == 1){
        timer2->start(1000);
        socket->connectToHost("192.168.1.188", 10086);
        //dia->close();
        flag = 0;}
    QString sendbuf = "begin";
    QByteArray arr;
    arr.append(sendbuf);
    socket->write(arr);
}


void MainWindow::on_cameraButton_clicked()//显示视频
{
    camera->show();
}

void MainWindow::on_tempButton_clicked()
{
    ui->maxtemp->setText(ui->tempEdit->text());
    QString sendbuf = "maxtmp,";
    sendbuf = sendbuf + ui->tempEdit->text();
    QByteArray arr;
    arr.append(sendbuf);
    socket->write(arr);
    qDebug()<< sendbuf;
}

void MainWindow::on_humiButton_clicked()
{
    ui->maxhumi->setText(ui->humiEdit->text());
    QString sendbuf = "maxhum,";
    sendbuf = sendbuf + ui->humiEdit->text();
    QByteArray arr;
    arr.append(sendbuf);
    socket->write(arr);
    qDebug()<< sendbuf;
}

void MainWindow::on_lightButton_clicked()
{
    ui->maxlight->setText(ui->lightEdit->text());
    QString sendbuf = "minlight,";
    sendbuf = sendbuf + ui->lightEdit->text();
    QByteArray arr;
    arr.append(sendbuf);
    socket->write(arr);
    qDebug()<< sendbuf;
}

//打开风扇
void MainWindow::on_fanBtnSet_clicked()
{
    if(1 == flag1)
    {
        QString sendbuf = "openled";
        QByteArray arr;
        arr.append(sendbuf);
        socket->write(arr);
        flag1 = 0;
        ui->fanBtnSet->setText("关闭");
    }
    else
    {
        QString sendbuf1 = "closeled";
        QByteArray arr1;
        arr1.append(sendbuf1);
        socket->write(arr1);
        flag1 = 1;
        ui->fanBtnSet->setText("启动");
    }
}

void MainWindow::on_r_id_clicked()
{
    QString sendbuf = "rid,"+ui->id->text();
    QByteArray arr;
    arr.append(sendbuf);
    socket->write(arr);
    qDebug()<< "注册";
    ui->id->setText("请输入卡号");
}

void MainWindow::on_pushButton_2_clicked()
{
    QString sendbuf = "did,"+ui->id->text();
    QByteArray arr;
    arr.append(sendbuf);
    socket->write(arr);
    ui->id->setText("请输入卡号");
}
