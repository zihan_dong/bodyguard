#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    this->setFixedSize(377,300);//设置窗口大小，不可放大
    this->setWindowTitle("婴儿房小卫士");//设置窗口名字

   // this->setWindowIcon(QIcon(":/logo.ico"));//设置图标
    QPalette palette;
    palette.setBrush(QPalette::Background,QBrush(QPixmap(":/back1.jpg").scaled(this->size())));
    this->setPalette(palette);

    //pix.load(":/logo.png");
    //ui->label_3->setScaledContents(true);
    //ui->label_3->setPixmap(pix);
    QLabel *pBack = new QLabel(this);
    QMovie *movie = new QMovie();
    movie->setFileName(":/pig.jpg");
    pBack->setMovie(movie);
    pBack->setScaledContents(true);
    pBack->resize(377,150);
    movie->start();
    pBack->move(0, 0);
    ui->loginButton->setStyleSheet("background-color:rgb(85, 170, 255);color: white");
    ui->zhuce->setStyleSheet("background-color:rgb(85, 170, 255);color: white");
    ui->username->setFocus();//光标跳至用户名栏开头处
    ui->passward->setEchoMode(QLineEdit::Password);//让password为小黑点
    socket = new QTcpSocket(this);
    socket->connectToHost("192.168.1.188", 10086);
    connect(socket,SIGNAL(readyRead()),this,SLOT(slotRecvData()));
    connect(socket,SIGNAL(connected()),this,SLOT(connect_solt()));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_zhuce_clicked()
{
    bool ok;
    QString username;
    QString userpassword;
    QString sendbuf;
    QByteArray arr;
    username = QInputDialog::getText(this,"用户名","请输入用户名",QLineEdit::Normal,"aaa",&ok);
    userpassword = QInputDialog::getText(this,"密码","请输入密码",QLineEdit::Normal,"aaa",&ok);
    sendbuf = "zhuce,"+username+","+userpassword;
    arr.append(sendbuf);
    socket->write(arr);
}

void Dialog::slotRecvData()
{
    QString succed;
    QByteArray arrer;
    QString admin = "admin";
    QString user = "user";
    arrer = socket->readAll();
    succed = QString(arrer);
    if("ZhuceSucced" == succed)
    {
        QMessageBox::information(this, "注册", "注册成功!");
        ui->username->clear();
        ui->passward->clear();

    }
    if("ZhuceError" == succed)
    {
        QMessageBox::information(this, "注册", "用户名已存在!请重新注册");
        ui->username->clear();
        ui->passward->clear();
    }

    if("adminLoginSucced" == succed)
    {
        QMessageBox::information(this, "登录", "管理员登录成功!");
        emit mySignal(admin);
        ui->username->clear();
        ui->passward->clear();

    }
    else if ("userLoginSucced" == succed)
    {
        QMessageBox::information(this, "登录", "用户登录成功!");
        emit mySignal(user);
        ui->username->clear();
        ui->passward->clear();

    }
    //else if ("LoginError" == succed)
    else
    {
        QMessageBox::information(this, "登录", "账号密码错误!");
        ui->username->clear();
        ui->passward->clear();
        ui->username->setFocus();
    }

}

void Dialog::on_loginButton_clicked()
{
    QString name = ui->username->text();
    QString password = ui->passward->text();
    QString sendbuf;
    sendbuf = "login,"+name+","+password;
    QByteArray arr;
    arr.append(sendbuf);
    socket->write(arr);
}
void Dialog::connect_solt()
{
    qDebug()<<"connect succeed";
}
