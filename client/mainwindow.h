#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "dialog.h"
#include <QValidator.h>
#include <QString>
#include <QTime>
#include "camerawidget.h"
#include <QDate>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();



private slots:



    void on_actionAbout_triggered();
    void changeWidgetSlot(QString);
    void on_pushButtonStop_clicked();
    void on_pushButtonStart_clicked();
    void slotRecvData();
    void updateTimeSlot();
    void on_cameraButton_clicked();
    void on_tempButton_clicked();
    void on_humiButton_clicked();
    void on_lightButton_clicked();
    void on_fanBtnSet_clicked();
    void on_r_id_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    QTcpSocket *socket;
    QTimer *timer1;
    QPixmap *pixmap;
    Dialog * dia;
    cameraWidget *camera;
    QTimer *timer;
    QTimer *timer2;
    QTime time;
    int flag;
    int flag1;
};

#endif // MAINWINDOW_H
