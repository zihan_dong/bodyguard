#include "camerawidget.h"
#include "ui_camerawidget.h"
#include <stdlib.h>
#include <unistd.h>
cameraWidget::cameraWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::cameraWidget)
{
    ui->setupUi(this);
    socket = new QTcpSocket(this);

    this->setWindowTitle("摄像监控");//设置窗口名字
    this->setWindowIcon(QIcon(":/logo.ico"));//设置图标
    //this->setFixedSize(320,300);
    //ui->label->resize(320,240);
    ui->label->move(0,0);
    flag = false;
    flag1 = 1;
    cameraSize = 0;
    state = 1;
    connect(socket,SIGNAL(readyRead()),this,SLOT(slotRecvData()));
}

cameraWidget::~cameraWidget()
{
    delete ui;
}
#define REQ_DATA_SIZE   32
#define HDR_DATA_SIZE   128
void cameraWidget::slotRecvData()
{
#if 0
    int ret;
    char response[HDR_DATA_SIZE] = {0};

    //qDebug() << "recv data";

    switch (state) {
    case 1:
        if (socket->bytesAvailable() < sizeof(response))
            return;

        ret = socket->read(response, sizeof(response));
        if (ret != sizeof(response)) {
            qDebug() << "recv response failed";
            goto err;
        }
        //qDebug()<<"res = "<<response;
        ret = sscanf(response, "%dlen", &piclen);
        //qDebug()<<"ret = "<<ret;

        if (ret != 1) {
            qDebug() << "response format error";
            goto err;
        }

        state = 2;
        //qDebug() << "piclen: " << piclen;

        if (socket->bytesAvailable() >= piclen)
            goto readdat;
        break;
    case 2:
 readdat:
        if (socket->bytesAvailable() < piclen)
            return;

        if (piclen > 0) {
            unsigned int ret;
            memset(picbuf, 0, sizeof(picbuf));
            ret = socket->read(picbuf, piclen);
            //qDebug()<<"ret111 = "<<ret;

            if (ret < piclen) {
                qDebug() << "recv pic failed";
                //timer->stop();
                socket->close();
                state = 0;
            }

            QPixmap pixmap;
            pixmap.loadFromData((const uchar *)picbuf, piclen, "JPEG");
            ui->label->setPixmap(pixmap);


        }

        state = 1;
        break;
    }

    return;

err:
    socket->close();
    state = 0;

    return;
#endif
    if(!readhand)
    {
        imageDate.clear();   //第一个数据包发来时，先清空存储图片数据的空间
        QByteArray by=socket->readAll();
        avalibleNum=by.left(10).toLong();   //找出第一个数据包的前10个字节,由此获得数据总长度。
        by.remove(0,10);  //移除前10个字节
        imageDate.append(by);
        readhand=true;
        if(imageDate.length()>=avalibleNum)
        {
            //判断数据是否接收完毕
            //qDebug()<<"receive a image head,length="<<avalibleNum;
            //changeFace();
            slot_replyFinished();
            readhand=false;
        }
    }
    else
    {
         QByteArray by=socket->readAll();
         imageDate.append(by);
         if(imageDate.length()>=avalibleNum)
         {
             //判断数据是否接收完毕
             //qDebug()<<"receive a image else,length="<<avalibleNum;
             //changeFace();
             slot_replyFinished();
             readhand=false;
         }
    }

}


void cameraWidget::on_pushButton_2_clicked()
{

         socket->connectToHost("192.168.1.188", 10086);
         flag1 = 0;
         QString sendbuf = "camera";
         QByteArray arr;
         arr.append(sendbuf);
         socket->write(arr);

}

void cameraWidget::on_pushButton_clicked()
{
    socket->close();
    this->close();
}
void cameraWidget::slot_replyFinished()
{
    QPixmap pix;

    pix.loadFromData(imageDate,"jpg");
    ui->label->setPixmap(pix);
    i++;
    qDebug()<<"接收第"<<i<<"张图片";

}
