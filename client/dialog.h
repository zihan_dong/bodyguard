#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <qmessagebox.h>
#include <qtextcodec.h>
#include <QPixmap>
#include <QMovie>
#include <QtNetwork/QTcpSocket>
#include <QString>
#include <QInputDialog>
#include <QDebug>
#include <QMessageBox>
#include <QPalette>
#include <QBrush>
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:

    void on_zhuce_clicked();
    void slotRecvData();
    void connect_solt();
    void on_loginButton_clicked();
signals:
    void mySignal(QString);
private:
    Ui::Dialog *ui;
    QPixmap pix;
    QMovie movie;
    QTcpSocket *socket;
};

#endif // DIALOG_H
