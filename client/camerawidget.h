#ifndef CAMERAWIDGET_H
#define CAMERAWIDGET_H

#include <QWidget>
#include <QtNetwork/QTcpSocket>
#include <QString>
#include <QDebug>
#include <QPalette>
#include <QImage>
#include <QBrush>
#include <QTimer>
#include <QLabel>
#include <QHostAddress>
#include <QMessageBox>
namespace Ui {
class cameraWidget;
}

class cameraWidget : public QWidget
{
    Q_OBJECT

public:
    explicit cameraWidget(QWidget *parent = 0);
    ~cameraWidget();

private slots:
        void slotRecvData();
        void on_pushButton_2_clicked();
        void slot_replyFinished();
        void on_pushButton_clicked();

private:
    Ui::cameraWidget *ui;
    QTcpSocket *socket;
    qint64 cameraSize;
    QByteArray fileData;
    bool flag;
    QTimer *timer;
    QTimer *timer1;
    unsigned int state;
    unsigned int piclen;
    char picbuf[1024 * 1024];
    int flag1;

    QByteArray imageDate;//存放图片数的QByteArray;
    QHostAddress hostAddress;//主机IP相关设置
    long avalibleNum;//存放图片大小
    bool readhand;
    void showIMage();//显示图片
    int i;//记录循环次数


};

#endif // CAMERAWIDGET_H
